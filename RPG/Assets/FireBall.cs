﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class FireBall : Attack {

	[SerializeField] GameObject fireBallProjectile;
	[SerializeField] float speed;
	[SerializeField] float attackDelay;

	private float attackTimer;
	
	public override void DoAttack (Vector2 target){
		if(attackTimer < 0){
			GameObject clone = (GameObject) Instantiate (fireBallProjectile, transform.position, Quaternion.identity);
			Vector3 targetVelocity = (target - (Vector2) transform.position).normalized * speed;
			clone.GetComponent <Rigidbody2D>().velocity = targetVelocity;
			attackTimer = attackDelay;
		}
	}

	void Update (){
		attackTimer -= Time.deltaTime;
	}
}
