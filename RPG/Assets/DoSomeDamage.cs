﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class DoSomeDamage : MonoBehaviour {

	[SerializeField] float damage;

	void OnCollisionEnter2D (Collision2D other){
		ExecuteEvents.Execute<IDamagable>(other.gameObject,null,(x,y) => x.DoDamage (damage));
		gameObject.SetActive (false);
	}


}
