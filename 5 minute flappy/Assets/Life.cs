﻿using UnityEngine;
using System.Collections;

public class Life : MonoBehaviour {

	[SerializeField] int life;

	public void OnCollisionEnter2D (Collision2D other){
		life = life - 1;
		if (life <= 0){
			Application.LoadLevel (Application.loadedLevel);
		}
	}

}
